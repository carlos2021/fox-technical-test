from .views import search_info_movie_view
from django.urls import path

urlpatterns = [
    path("search-info-movie/<str:movie>/", search_info_movie_view, name="remove_vowels"),
]
