import aiohttp
import asyncio
import json
from typing import List, Dict, Any, Optional
from pydantic import BaseModel, Field, HttpUrl, ValidationError, field_validator
from collections import defaultdict



class ActorCount(BaseModel):
    name: str
    count: int

class ResponseAgroupDataOmb(BaseModel):
    year: str
    actors: List[ActorCount]


class Movie(BaseModel):
    title: str = Field(..., alias='Title')
    year: str = Field(..., alias='Year')
    imdb_id: str = Field(..., alias='imdbID')
    type: str = Field(..., alias='Type')
    poster: Optional[HttpUrl] = Field(None, alias='Poster')
    actors: Optional[List[str]] = []

    @field_validator('poster')
    def poster(cls, v):
        if v == 'N/A':
            return None
        return v

class MoviesResponse(BaseModel):
    search: List[Movie] = Field(..., alias='Search')
    total_results: str = Field(..., alias='totalResults')
    response: str = Field(..., alias='Response')


class MovieDetails(BaseModel):
    genre: str = Field(..., alias='Genre')
    director: str = Field(..., alias='Director')
    writer: str = Field(..., alias='Writer')
    actors: str = Field(..., alias='Actors')
    
       

class OMDBClient:
    BASE_URL = "http://www.omdbapi.com/"

    def __init__(self, api_key: str, request_delay: float = 0.2) -> None:
        self.api_key = api_key
        self.request_delay = request_delay

    async def fetch_movies(self, word_to_search: str) -> List[Movie]:
        """Fetch all movies related to the given word."""
        all_movies = []
        page = 1

        async with aiohttp.ClientSession() as session:
            while True:
                response = await self._fetch_movies_by_search_term(session, word_to_search, page)
                if response.get("Response") == "False":
                    break

                try:
                    movies_response = MoviesResponse(**response)
                    all_movies.extend(movies_response.search)
                except ValidationError as e:
                    print(f"Validation error: {e}")
                    break

                total_results = int(movies_response.total_results)
                if len(all_movies) >= total_results:
                    break

                page += 1
                await asyncio.sleep(self.request_delay)

        return all_movies

    async def _fetch_movies_by_search_term(
        self, session: aiohttp.ClientSession, term: str, page: int
    ) -> List[Movie]:
        """Fetch movies by search term and page number."""
        params = {"apikey": self.api_key, "s": term, "type": "movie", "page": page}
        async with session.get(self.BASE_URL, params=params) as response:
            return await response.json()
        
    async def _fetch_movie_by_imdb_id(
        self, session: aiohttp.ClientSession, imdb_id: str
    ) -> Dict[str, Any]:
        """Fetch a movie by IMDb ID."""
        params = {"apikey": self.api_key, "i": imdb_id}
        async with session.get(self.BASE_URL, params=params) as response:
            return await response.json()
        
    async def fetch_movie_by_imdb_id(self, imdb_id: str) -> Optional[MovieDetails]:
        """Fetch a single movie by IMDb ID."""
        async with aiohttp.ClientSession() as session:
            response = await self._fetch_movie_by_imdb_id(session, imdb_id)
            return MovieDetails(**response)
        
    async def get_data_by_search_term(self, term: str) -> List[ResponseAgroupDataOmb]:
        """Fetch a single movie by IMDb ID."""
        async with aiohttp.ClientSession() as session:
            movies = await self.fetch_movies(term)
            tasks = []
            movies_count = defaultdict(lambda: defaultdict(int))
            for movie in movies:
                task = self.fetch_movie_by_imdb_id(imdb_id=movie.imdb_id)
                tasks.append(task)
            responses = await asyncio.gather(*tasks)
            
            for movie, response_detail_movie in zip(movies, responses):
                if response_detail_movie:
                    list_actors = response_detail_movie.actors.split(", ")
                    movie.actors = list_actors
                    for actor in list_actors:
                        movies_count[movie.year][actor] += 1
            json_output = []
            for year, actors_count in movies_count.items():
                year_data = {
                    "year": year,
                    "actors": [{"name": actor, "count": count} for actor, count in actors_count.items()]
                }
                json_output.append(year_data)
            data_by_years: List[ResponseAgroupDataOmb] = json_output 
            return data_by_years
            
