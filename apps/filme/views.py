from django.http import JsonResponse
from .utilites.omb_client import OMDBClient, ResponseAgroupDataOmb
from adrf.decorators import api_view


@api_view(["GET"])
async def search_info_movie_view(request, movie) -> ResponseAgroupDataOmb:
    api_key = "b34c2938"
    client = OMDBClient(api_key)
    response_agroup_data_omb = await client.get_data_by_search_term(movie)
    return JsonResponse(response_agroup_data_omb, safe=False)
