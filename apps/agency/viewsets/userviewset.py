from rest_framework import viewsets
from apps.agency.serializers.userSerializer import UserSerializer
from apps.agency.models import User

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    