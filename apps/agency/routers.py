from rest_framework.routers import DefaultRouter
from apps.agency.viewsets.propertyviewset import PropertyViewSet
from apps.agency.viewsets.userviewset import UserViewSet
from apps.agency.viewsets.groupviewset import GroupViewSet
from apps.agency.viewsets.permissionviewset import PermissionViewSet

router = DefaultRouter()

router.register("property", PropertyViewSet, basename="property")
router.register("user", UserViewSet, basename="user")
router.register("group", GroupViewSet, basename="group")
router.register("permission", PermissionViewSet, basename="permission")

urlpatterns = router.urls
