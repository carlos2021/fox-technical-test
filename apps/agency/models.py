from django.db import models
from django.contrib.auth.models import AbstractUser, Group, Permission
from django.conf import settings


class User(AbstractUser):
    is_seller = models.BooleanField(default=False)
    is_buyer = models.BooleanField(default=False)
    groups = models.ManyToManyField(
        Group,
        related_name="agency_user_set",
        blank=True,
        help_text="The groups this user belongs to.",
        verbose_name="groups",
    )
    user_permissions = models.ManyToManyField(
        Permission,
        related_name="agency_user_set",
        blank=True,
        help_text="Specific permissions for this user.",
        verbose_name="user permissions",
    )

    class Meta:
        db_table = "user"


class Property(models.Model):
    COMMERCIAL = "comercial"
    RESIDENTIAL = "residencial"
    OFFICE = "oficinas"

    PROPERTY_TYPE_CHOICES = [
        (COMMERCIAL, "Comercial"),
        (RESIDENTIAL, "Residencial"),
        (OFFICE, "Oficinas"),
    ]
    name = models.CharField(max_length=100)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    property_type = models.CharField(max_length=50, choices=PROPERTY_TYPE_CHOICES)
    owner = models.ForeignKey(
        User, related_name="properties", on_delete=models.CASCADE, unique=True
    )

    class Meta:
        db_table = "property"


class Transaction(models.Model):
    buyer = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="purchases"
    )
    property = models.ForeignKey(Property, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        db_table = "transaction"
