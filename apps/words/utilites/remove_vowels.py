def remove_vowels(input_string: str) -> str:
    """Remove vowels from a given string."""
    vowels: set[str] = set("aeiouAEIOU")
    return ''.join([char for char in input_string if char not in vowels])
