from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.http import JsonResponse

from .utilites.remove_vowels import remove_vowels

@api_view(['GET'])
def remove_vowels_view(request, word):
    string_without_vowels: str = remove_vowels(input_string = word)
    return JsonResponse({'result': string_without_vowels})