from .views import remove_vowels_view
from django.urls import path

urlpatterns = [
    path("remove-vowels/<str:word>/", remove_vowels_view, name="remove_vowels"),
]
