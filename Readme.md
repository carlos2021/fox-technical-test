# TECHNICAL TEST FOX

## 1 ) En el lenguaje de su preferencia, implemente una función que, dado un string, elimine las vocales y retorne el string resultante.

## 2 ) Usando React, cree un proyecto que consuma la siguiente API:https://www.omdbapi.com/ . Con los datos de la API, agrúpelos por año y actor, y entregue un JSON con la cuenta total de películas por año y actor. Es importante consumir todas las páginas de la API.

## 3 ) Mencione brevemente qué opciones de escalabilidad existen en software para aumentar el rendimiento de una base de datos. R/ respuesta al final

## 4 ) Diseñe un modelo relacional para un sistema de compra y venta de inmuebles con las siguientes restricciones:
- Un vendedor puede tener varios inmuebles.
- Existen varios tipos de inmuebles (comercial, residencial, oficinas).
- Un inmueble sólo puede tener un propietario.
- Los usuarios tienen un esquema de permisos a nivel de rol y usuario.
- La tabla de usuarios es la misma para el comprador y el vendedor.


# RESPUESTAS

### 1 R/ \apps\words

### 2 R/ \apps\filme

### 3 R/ \documentation\escalardb

### 4 R/ \documentation\der point 3.pdf y \apps\agency



![Texto Alternativo](documentation/example_run.png)


# README 

Este es el backend de la prueba tecnica de fox

## Settings

- Realiza la configurancion de las variables de entorno en el archivo .env
- Cree un entorno virtual
- corra python manage.py runserver

## Docker

El proyecto esta fuertemente cimentado en el uso de Docker, y docker-compose, tan para los entornos de desarrollo, como producción. Para mas detalles, verificar los comandos en [cookiecutter-django Docker documentation](http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html).

## Comandos basicos de trabajo

    
### Migraciones

    $ docker-compose -f local.yml run --rm django python manage.py makemigrations
    $ docker-compose -f local.yml run --rm django python manage.py migrate

### Validación de tipado

Running type checks with mypy:

    $ docker-compose -f local.yml run --rm django mypy testfox

### Análisis de código estatico

El proyecto incluye Flake8, Bandit, y PyLint, para los dos primeros, solo es correr el proceso directamente en el archivo del proyecto. Para el caso de PyLint, si se hace necesaria la configuración de las variables del proyecto, por lo que se corre directamente en Docker.

    $ docker-compose -f local.yml run --rm django pylint testfox

### Cobertura de pruebas

Para correr las pruebas, es importante usar la cobertura de los mismos, lo que ademas permite visualizar en resultado en un reporte HTML:

    $ docker-compose -f local.yml run --rm django coverage run -m pytest
    $ docker-compose -f local.yml run --rm django coverage html
    $ open htmlcov/index.html

#### Correr los test usando pytest

    $ docker-compose -f local.yml run --rm django pytest


## Despliegue a producción

Para mayor información sobre el despliegue del proyecto en entornos de producción, revisar la documentación [usando Docker](https://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html)
