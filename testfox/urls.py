from django.contrib import admin
from django.urls import path, include
from testfox.config_swagger import urlpatternsSwagger


urlpatterns = urlpatternsSwagger + [
    path("admin/", admin.site.urls),
    path("point1-words/", include("apps.words.routers")),
    path("point2-filme/", include("apps.filme.routers")),
    path("point4-agency/", include("apps.agency.routers")),
]
